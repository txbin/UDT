import { mainWindow } from "./index";

const electron = require("electron");
// const ipc = electron.ipcMain;
// const BrowserWindow = electron.BrowserWindow;
const Menu = electron.Menu;
const app = electron.app;

let template = [
  {
    label: "File",
    role: "file",
    submenu: [
      {
        label: "Save test results",
        click: function() {
          mainWindow.webContents.send("saveResult", "1");
        }
      }
    ]
  },
  {
    label: "View",
    submenu: [
      { role: "reload" },
      { role: "forcereload" },
      { role: "toggledevtools" }
    ]
  },
  {
    role: "help",
    submenu: [
      {
        label: "Learn More",
        click() {
          require("electron").shell.openExternal(
            "http://www.dingwave.com/bbs/forum.php"
          );
        }
      }
    ]
  }
];

app.on("ready", function() {
  const menu = Menu.buildFromTemplate(template);
  Menu.setApplicationMenu(menu);
});

const ipc = require("electron").ipcMain;
const dialog = require("electron").dialog;
ipc.on("save-dialog", function(event) {
  const options = {
    title: "save test Result",
    filters: [{ name: "Excel", extensions: ["xlsx"] }]
  };
  dialog.showSaveDialog(options, function(filename) {
    if (!filename) {
      filename = "";
    }
    event.returnValue = filename;
  });
});
