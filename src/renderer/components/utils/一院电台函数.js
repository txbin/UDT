//引入串口操作类
import serialport from 'serialport'

class SerialportObj {
  //构造器
  constructor({ port, baudrate, ...params }) { //创建串口操作对象
    this._portHandle = new serialport(port, baudrate, params);
  }

  /**
   *注册心跳帧监听器
   *
   * @param {} frameType
   * @memberof SerialportObj
   */
  creatHeartMonitor(frameType) {
    this._portHandle.on("msg", () => {
      let isHeartFrame = parseFrameType(msg, frameType); //帧类型过滤器
      if (isHeartFrame) doHeartBusiness(); //符合帧类型,处理心跳帧
    })
  }

  /**
   *注册上报帧监听器
   *
   * @param {*} frameType
   * @memberof SerialportObj
   */
  creatReportMonitor(frameType) {
    this._portHandle.on("msg", () => {
      let isReportFrame = parseFrameType(msg, frameType); //帧类型过滤器
      if (isReportFrame) doReportBusiness() //符合帧类型,处理上报帧
    })
  }

  /**
   *配置电台参数
   *
   * @param {Buffer} cfgData 配置电台数据
   * @param {int} frameType 期望回读的帧类型
   * @param {Number} timeout  回读响应数据超时时间
   * @returns 配置后响应数据
   * @memberof SerialportObj
   */
  async cfgRadioParms(cfgData, frameType, timeout) {
    let that = this;
    this._portHandle.write(cfgData); //写入串口（配置电台参数）
    //创建响应帧监听器,在此处创建函数目的是为了方便垃圾回收，当前块函数执行完后即准备释放内存
    const creatCfgMonitor = async (frameType) => {
      that._portHandle.on("msg", () => {
        return new Promise((resolve, reject) => {
          setTimeout(resolve, _Data.timeout, ""); //接收超时，返回接收为空字符
          let isReportFrame = parseFrameType(msg, frameType); //帧类型过滤器
          if (isReportFrame) resolve(msg); //收到期望响应帧，返回数据
        })
      })
    }
    let responseFromRadio = await creatCfgMonitor(frameType); //调用响应帧监听器
    return responseFromRadio;
  }
  //向串口写入数据
  writeData(data) {
    this._portHandle.write(data);
  }
}
export { SerialportObj }