const dataTools = {
	hexStrAddSpace: (sendData) => {
		let spaceSendData = "";
		if (sendData) {
			sendData.match(/[\da-f]{2}/gi).map(function (h) {
				spaceSendData = spaceSendData + h + " ";
			});
		}
		return spaceSendData;
	},
	hexStr2Arry(sendDataStr) {
		let sendArry = [];
		sendDataStr.match(/[\da-f]{2}/gi).map(function (h) {
			sendArry.push(parseInt(h, 16));
		});
		return sendArry;
	},
	getVerify: hex => {
		let verifyStr = "";
		let typedArray=[];
		hex.match(/[\da-f]{2}/gi).map((h) => {
			console.log(parseInt(h, 16));
			typedArray.push(parseInt(h, 16))
		});
		console.log(`typedArray${typedArray}`, typedArray)

		let sum = 0;
		for (let i = 0; i < typedArray.length; i++) {
			sum = sum + typedArray[i];
			console.log(sum)
		}

		let EndValue = ~(sum & 0xff) + 1; //取出低八位并按位取反+1
		let endUnsignStr = (EndValue >>> 0).toString(16); //有符号转换为无符号32位，
		verifyStr = endUnsignStr.substring(endUnsignStr.length - 2); //截取最后的2为校验符号值
		console.log("校验计算值：", verifyStr);
		return verifyStr;
	},
}
export {
	dataTools
}

// let a = "a5da1e000200000022212f96080000ffffff0fff0f00000000ff3f0000f9"
// console.log(dataTools.getVerify(a)==0)