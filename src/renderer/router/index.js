import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  routes: [{
    path: "/",
    name: "landing-page",
    component: require("@/components/index").default,
    children: [{
      path: "/udpServer",
      name: "udpServer",
      component: require("@/components/pages/udpServer").default,
    }, {
      path: "/CRCVerify",
      name: "CRCVerify",
      component: require("@/components/pages/CRCVerify").default,
    },{
      path: "/update",
      name: "update",
      component: require("@/components/pages/autoUpdate").default,
    }
    ]
  },
  {
    path: "*",
    redirect: "/"
  }
  ]
});